// Imports
var Client = require('fabric-client');
var util = require('util');
const config = require('./config');

// Config from network.yaml (must be downloaded from OBP)
var client = Client.loadFromConfig('network_org1.yaml');
var fabric_ca_client = null;

// Main
client.initCredentialStores()
.then((nothing) => {
    channel = client.getChannel(config.CHANNEL_NAME);
    //console.log(util.inspect(client));

    fabric_ca_client = client.getCertificateAuthority();

    return client.setUserContext({username: 'rca-org1-admin', password: 'rca-org1-adminpw'})
    })
.then((admin) => {

    console.log('Running registration query with this admin user: '+admin.getName());

     return fabric_ca_client.register({enrollmentID: config.OFFLINE_USER, enrollmentSecret: config.OFFLINE_PASSWORD, role: 'client', affiliation: 'org1'}, admin)
    })
.then((secret) => {
        console.log('Secret to use for '+config.OFFLINE_USER+':'+secret);
    });


