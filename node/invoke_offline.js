//
// Imports
//
var Client = require('fabric-client');
var path = require('path');
var util = require('util');
var fs = require('fs');
const crypto = require('crypto');
const elliptic = require('elliptic');
const { KEYUTIL } = require('jsrsasign');
const config = require('./config');

//
// Script configuartion variables
//
var fcn = 'set';
var args = ["a","offline transaction"];
var priv = fs.readFileSync(config.PRIV, 'utf8');
var { prvKeyHex } = KEYUTIL.getKey(priv,'wisekey'); 
var cert = fs.readFileSync(config.CERT, 'utf8');
const EC = elliptic.ec;
const ecdsaCurve = elliptic.curves['p256'];
const ecdsa = new EC(ecdsaCurve);
const signKey = ecdsa.keyFromPrivate(prvKeyHex, 'hex');

//
// Config init
//
var client = Client.loadFromConfig('network_org1.yaml');
var targets = client.getPeersForOrg('org1');

//
// Helper function
//
function _preventMalleability(sig) {
	const halfOrder = elliptic.curves.p256.n.shrn(1);
	if (sig.s.cmp(halfOrder) === 1) {
		const bigNum = elliptic.curves.p256.n;
		sig.s = bigNum.sub(sig.s);
	}
	return sig;
}

// 
// Main
// 
client.initCredentialStores()
.then((nothing) => {

channel = client.getChannel(config.CHANNEL_NAME);

// 1. Generate unsigned transaction proposal
var transaction_proposal = {
  chaincodeId: config.CHAINCODE_NAME,
  channelId: config.CHANNEL_NAME,
  fcn: fcn,
  args: args,
};

var { proposal, tx_id } = channel.generateUnsignedProposal(transaction_proposal, 'org1', cert);

// 2. Hash the transaction proposal
var proposalBytes = proposal.toBuffer();
var digest = client.getCryptoSuite().hash(proposalBytes);
 
// 3. Calculate the signature for this transacton proposal
console.log("digest: "+digest);   
console.log("signKey: ");
console.log(util.inspect(signKey));
var sig = ecdsa.sign(Buffer.from(digest, 'hex'), signKey);
sig = _preventMalleability(sig);
var signature = Buffer.from(sig.toDER());
var signedProposal = {
  signature,
  proposal_bytes: proposalBytes,
};

// 4. Send the signed transaction proposal
var proposal_request = {
  signedProposal: signedProposal,
  targets: targets
}
var proposalResponses = null;
var transaction_request = null;
channel.sendSignedProposal(proposal_request)
.then((responses) => {
    proposalResponses = responses;
    console.log('Proposal responses:');
    console.log(util.inspect(proposalResponses));

    // 5. Generate unsigned transaction
    transaction_request = {
      proposalResponses: proposalResponses,
      proposal: proposal,
    };
    var commitProposal = channel.generateUnsignedTransaction(transaction_request);
    return commitProposal;
})  
.then((commitProposal) => {

    // 6. Sign the unsigned transaction
    console.log('commitProposal:');
    console.log(util.inspect(commitProposal));
    var transactionBytes = commitProposal.toBuffer();
    var transaction_digest = client.getCryptoSuite().hash(transactionBytes);
    var transaction_sig = ecdsa.sign(Buffer.from(transaction_digest, 'hex'), signKey);        
    transaction_sig = _preventMalleability(transaction_sig);
    var transaction_signature = Buffer.from(transaction_sig.toDER());

    var signedTransactionProposal = {
      signature: transaction_signature,
      proposal_bytes: transactionBytes,
    };

    var signedTransaction = {
      signedProposal: signedTransactionProposal,
      request: transaction_request,
    }

    console.log('Transaction request sent to the orderer:');
    console.log(util.inspect(signedTransaction));
    
    // 7. Commit the signed transaction
    return channel.sendSignedTransaction(signedTransaction);
})
.then((response) => {
    console.log('Successfully sent transaction');
    console.log('Return code: '+response.status);
});

});
