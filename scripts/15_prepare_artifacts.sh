cp -r /tmp/hyperledger/org1/msp/admincerts /tmp/hyperledger/org1/admin/msp/
mkdir -p /tmp/hyperledger/org1/peer1/assets/chaincode
cp -r ./src/sacc /tmp/hyperledger/org1/peer1/assets/chaincode/
cp /tmp/hyperledger/org0/orderer/channel.tx /tmp/hyperledger/org1/peer1/assets/channel.tx
cp -r ./scripts /tmp/hyperledger/org1/admin/

cp -r /tmp/hyperledger/org2/msp/admincerts /tmp/hyperledger/org2/admin/msp/
cp -r ./scripts /tmp/hyperledger/org2/admin/

