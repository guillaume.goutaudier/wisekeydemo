export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/org1/ca/crypto/ca-cert.pem
export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/org1/ca/admin
export FABRIC_CA_CLIENT_MSPDIR=msp
./bin/fabric-ca-client enroll -d -u https://rca-org1-admin:rca-org1-adminpw@0.0.0.0:6053
./bin/fabric-ca-client register -d --id.name wisekey --id.secret wisekeyPW --id.type client -u https://0.0.0.0:6053

