//
// Imports
//
var Client = require('fabric-client');
var path = require('path');
var util = require('util');

//
// Config from network.yaml (must be downloaded from OBP)
//
var client = Client.loadFromConfig('network.yaml');
var client_org = client.getClientConfig().organization;
var targets = client.getPeersForOrg(client_org);

// 
// Main
// 
client.initCredentialStores()
.then((nothing) => {
    channel = client.getChannel('default');
    //console.log(util.inspect(client));

    client.setUserContext({username: 'demouser', password: 'zigaiL3UShoo3fee!'})
.then((user) => {

    console.log('Using user: '+user.getName());

    tx_id = client.newTransactionID(true);

    var proposal_request = {
      targets: targets,
      chaincodeId: 'sacc',
      fcn: 'set',
      args: ["a","60"],
      txId: tx_id
    };

    channel.sendTransactionProposal(proposal_request).then((response_payloads) => {
        var proposalResponses = response_payloads[0];
        var proposal = response_payloads[1];

        var transaction_request = {
          proposalResponses: proposalResponses,
          proposal: proposal,
          txId: tx_id,
        };


        console.log("Response from peers:");
        console.log(util.inspect(response_payloads));

        console.log("Transaction request sent to the orderer:");
        console.log(util.inspect(transaction_request));


        channel.sendTransaction(transaction_request).then((results) => {
          console.log('Successfully sent transaction');
        });
    });

});

});


