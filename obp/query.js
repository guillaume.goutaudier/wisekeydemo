//
// Imports
//
var Client = require('fabric-client');
var path = require('path');
var util = require('util')

// Using loadFromConfig to get network topology
// https://hyperledger.github.io/fabric-sdk-node/release-1.4/tutorial-network-config.html
var client = Client.loadFromConfig('network.yaml');
var client_org = client.getClientConfig().organization;
var targets = client.getPeersForOrg(client_org);
console.log(util.inspect(targets));

// 
// Main
// 
var request = {
  targets: targets,
  chaincodeId: 'sacc',
  fcn: 'get',
  args: ["a"]
};

client.initCredentialStores()
.then((nothing) => {
    channel = client.getChannel('default');
    //console.log(util.inspect(client));

    client.setUserContext({username: 'demouser', password: 'zigaiL3UShoo3fee!'})
.then((user) => {

    console.log('Using user: '+user.getName());

    channel.queryByChaincode(request, true).then((response_payloads) => {
        for(let i = 0; i < response_payloads.length; i++) {
            console.log(util.format('Query result from peer [%s]: %s', i, response_payloads[i].toString('utf8')));
        }
    }, (err) => {
                console.log('Failed to send query due to error: ' + err.stack ? err.stack : err);
                throw new Error('Failed, got error on query');
        });
    });
});


